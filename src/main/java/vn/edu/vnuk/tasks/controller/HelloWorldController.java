package vn.edu.vnuk.tasks.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorldController {
	
	@RequestMapping("/hellospring")
	public String execute (ModelMap modelMap) {
		modelMap.put("greeting","Hello Spring");
		return "hello-spring";
	}
	
	@RequestMapping(value="/helloworld")
	public String execute() {
		return "hello";
	}
	
}
